import React from 'react';

import './sign-in-and-sign-out.styles.scss';

import SignIn from '../../components/sign-in/sign-in.component';

const SignInAndSignUpPage = () => {
	return (
		<div>
			<SignIn />
		</div>
	);
};

export default SignInAndSignUpPage;
