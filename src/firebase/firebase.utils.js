import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
        apiKey: "AIzaSyCpuYs7O3AEdOf1Zuy6EX97etlSbppq5Ag",
        authDomain: "e-commerceapp-7884b.firebaseapp.com",
        databaseURL: "https://e-commerceapp-7884b.firebaseio.com",
        projectId: "e-commerceapp-7884b",
        storageBucket: "",
        messagingSenderId: "829619570315",
        appId: "1:829619570315:web:89172f7ceab0211cb6b68e"
}

firebase.initializeApp(config)

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;